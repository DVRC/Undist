#pragma once

#define when \
	break; \
	case

#define fallback \
	break; \
	default

#define new(ptr) \
	((ptr) = malloc(sizeof(*(ptr))))

#define new_array(ptr, size) \
	((ptr) = calloc((size), sizeof( *(ptr) )))

#define new_str(str, size) new_array((str), (size)+1)

#define resize(ptr, size) \
	((ptr) = realloc((ptr), (size) * sizeof( *(ptr) )))

#define static_len(array) \
	(sizeof((array)) / sizeof(*(array)))
