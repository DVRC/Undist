#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "macros.h"
#include "strutils.h"
#include "err.h"

/* Read the whole line, char by char, until it finds a \n.
 * Then terminate with a null terminator
 * This function allocates memory.
 */
//char* scanline(FILE *f, int *len) /* Good */
char* scanline(FILE *f) /* Good */
{
	char *line = NULL;
	int bufsize = BUF_CHUNK;
	char newchar = 0;
	int idx = 1; /* Must have one char more */

	if (new_array(line, bufsize) == NULL)
		error(errmsg[ERR_ALLOC]);

	/* Until the end of the line or the file */
	while ((newchar = fgetc(f)) != '\n' && newchar != '\0') {
		line[idx-1] = newchar;
		if (++idx > bufsize) {
			bufsize += BUF_CHUNK;
			if (resize(line, bufsize) == NULL)
				error(errmsg[ERR_GROW]);
		}
	}

	line[idx-1] = '\0'; /* Terminate the line */

	/* Resize to the exact size */
	if (resize(line, idx) == NULL)
		error(errmsg[ERR_SHRINK]);
	//*len = idx;

	return line;
}

/* Take the line and tokenize. If the token has parenthesis, the
 * spaces inside are discarded, and gets closed by a matching
 * parenthesis.
 * Returns the number of tokens created.
 * We cannot use the normal strtok for this reason.
 */
uint32_t tokenize(char *info)
{
	uint32_t open_par = 0,
		 toks = 1; /* At least one token exists */

	/* Empty string, then no tokens */
	if (*info == '\0')
		return 0;

	while (*info != '\0') {
		if (!open_par && *info == ' ') {
			*info = '\0';
			++toks;
		}
		else if (*info == '(')
			++open_par;
		else if (*info == ')' && open_par)
			--open_par;

		info++;
	}

	/* If there are still matching parenthesis, fail */
	if (open_par)
		error(errmsg[ERR_PARSE]);

	return toks;
}

/* Returns the next token in a null separated string. */
Token nexttok(char *info, int *toks)
{
	static char	*current = NULL,
			*next = NULL;
	static uint32_t toknum = 0;
	static Token t = (Token){NULL, 0};

	/* New string, we call with info */
	if (info) {
		next = info;
		toknum = *toks; /* Ugly hack */
	}
	current = next;

	t.cstr = current;

	/* Pending tokens */
	if (toknum) {
		while (*next != '\0')
			next++;
		t.len = next - current;
		toknum--; /* Processed token */
	}
	if (toknum) /* There are still pending tokens, take the next one */
		next++;

	return t;
}

/* Get the attribute inside parenthesis */
String getattr(char *tok)
{
	char	*start = tok,
		*end = NULL;

	String attr = {NULL, 0};

	/* Find the first parenthesis, and get to the next pos */
	while (*start != '(' && *start != '\0')
		start++;

	/* Check is not a terminator */
	if (*start == '\0')
		return attr; /* fail */

	/* Take the next char to the parenthesis */
	start++;

	/* Find the last parenthesis and put a terminator instead */
	end = start;
	while (*end != ')' && *end != '\0')
		end++;

	if (*end == '\0')
		return attr; /* Fail */

	*end = '\0';

	/* Save the crafted string */
	if (new_str(attr.cstr, (end - start)) == NULL)
		error(errmsg[ERR_ATTRIB]);

	strcpy(attr.cstr, start);
	attr.len = end - start;

	/* Restore the last parenthesis */
	*end = ')';

	return attr;
}
