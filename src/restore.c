#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <dirent.h>
#include <sys/stat.h>

#include "restore.h"
#include "hashtbl.h"
#include "macros.h"
#include "err.h"
#include "errno.h"

extern Token rbase, srcpath;
extern FILE *dist_load();

/* Funny things */
enum attribs {
	SIZE,
	CMPSIZE,
	SYMVAL,
	MACH
};

enum tok_type {
	TYPE,
	PERMS,
	OWNER,
	GROUP,
	//PATH,
	NAME,
	TAPEFILE,
	DIST
};

const char *attr_str[] = {
	[SIZE] = "size",
	[CMPSIZE] = "cmpsize",
	[SYMVAL] = "symval",
	[MACH] = "mach"
};


/* Create a table with metadatas of the files to extract. */
static Realfile rf_parse(char *line)
{
	Realfile rf = {
		.type = 'n',
		.perms = NULL,
		//.path = (String){NULL, 0},
		.dist = (String){NULL, 0},
		.size = 0, /* link should be NULL */
		.mach_spec = false
	};

	Token curtok = {
		.len = 0,
		.cstr = NULL
	};

	int tokens = 0;
	tokens = tokenize(line);

	/* For every token */
	for (int i = 0; i < tokens; ++i) {
		curtok = nexttok(i ? NULL : line, i ? NULL : &tokens);
		switch (i) {
		case TYPE:
			rf.type = *(curtok.cstr); /* Just a char */
			break;
		case PERMS:
			//rf.perms = curtok.cstr;
			rf.perms = curtok; /* Reference */
			break;
		case OWNER: /* Not implemented */
			continue;
			//rf.owner = curtok.cstr;
		case GROUP: /* Not implemented */
			continue;
			//rf.group = curtok.cstr;
#ifdef undef
		case PATH: /* Get the destination path */
			rf.skip = 2 + curtok.len;
			if (new_str(rf.path.cstr, rbase.len + curtok.len) == NULL)
				error(errmsg[ERR_ALLOC]);
			strcat(strcat(strcpy(rf.path.cstr, rbase.cstr), "/"), curtok.cstr);
			rf.path.len = rbase.len + curtok.len;
			break;
#endif
		case NAME:
			rf.name = curtok; /* Reference */
			break;
		case TAPEFILE:	/* Unused */
			continue;
		case DIST: /* Get the dist and add it to the DB */
		{
			int len = 0;
			char *cut = curtok.cstr;
			bool point = false;

			/* The distfile is usually foo.bar
			 * Inside the idb file we usually have foo.bar.baz,
			 * where baz is something indicating what's the file
			 * about. We don't need this last part.
			 */
			while (*(cut++) != '\0') {
				if (*cut == '.') {
					if (point) {
						*cut = '\0';
						break;
					}
					else
						point = true;
				}
				++len;
			}

			/* Alloc mem */
			if (new_str(rf.dist.cstr, len) == NULL)
				error(errmsg[ERR_ALLOC]);
			strcpy(rf.dist.cstr, curtok.cstr);
			rf.dist.len = len;
			*cut = '.';	/* Restore the point */
			//fprintf(stderr, "DEBUG: distfile str: %s\n", rf.dist.cstr);
		}
			break;
		default:
		{
			String tmp;
			/* WIP */
			if (strstr(curtok.cstr, attr_str[SIZE])) {
				tmp = getattr(curtok.cstr);
				if (tmp.cstr == NULL)
					error(errmsg[ERR_PARSE]);
				rf.size = atoi(tmp.cstr);
				free(tmp.cstr);
			}
			else if (strstr(curtok.cstr, attr_str[CMPSIZE])) {
				rf.type = 'c';
				tmp = getattr(curtok.cstr);
				if (tmp.cstr == NULL)
					error(errmsg[ERR_PARSE]);
				rf.size = atoi(tmp.cstr);
				free(tmp.cstr);
			}
			else if (strstr(curtok.cstr, attr_str[SYMVAL])) {
				if (new(rf.symval) == NULL)
					error(errmsg[ERR_ALLOC]);
				*(rf.symval) = getattr(curtok.cstr);
			}
			else if (strstr(curtok.cstr, attr_str[MACH])) {
				rf.mach_spec = true;
				//parse_machine(); /* TODO */
				/* Signal that the machine field is present. */
			}
			/* All the other tokens that aren't parsed:
			 * - preop, postop, exitop
			 * - $vhdev, @$instmode and X$instmode
			 * - config, mach, f
			 * - sum
			 */
		}
		}
	}

	return rf;
}

/* Cleanup file attributes after use
 * The strings used to manage a Realfile are dynamically allocated,
 * so they must be destroyed after use
 */
static void rf_clean(Realfile *rf)
{
	if (rf->dist.cstr) {
		free(rf->dist.cstr);
		rf->dist.len = 0;
	}
	if (rf->type == 'l' && rf->symval) {
		free(rf->symval->cstr);
		free(rf->symval);
	}
}

//void dir_restore(Realfile *rf)
/* TODO: check if the permission string is parsed correctly into an int */
//static void dir_restore(String path, int perm)
static void dir_restore(String path)
{
	DIR *d;
	int rel = path.len;

	/* This is pretty clever: we know which is the rbase and where
	 * we are going to extract the whole dist. So, there is for sure
	 * a common prefix.
	 */
	while ((d = opendir(path.cstr)) == NULL) {
		//fprintf(stderr, "DEBUG: path is %s\n", path.cstr);
		//fprintf(stderr, "DBG: rel %d : rbase %d : char %c\n",
		//	rel, rbase.len, path.cstr[rel]);
		while (rel-- > rbase.len && path.cstr[rel] != '/');
			;
		path.cstr[rel] = '\0';

	}
	closedir(d);

	while (rel < path.len) {
		//fprintf(stderr, "DEBUG: path is %s\n", path.cstr);
		if (path.cstr[rel] == '\0') {
			path.cstr[rel] = '/';
			/* Should inherit the parent permissions */
			mkdir(path.cstr, 0755);
		}
		rel++;
	}

	//if (perm > -1)
	//	chmod(path.cstr, perm);
}

static void link_restore(Realfile *rf)
{
#ifndef SYMVALS
	fprintf(stderr, "WARNING: link restoring not implemented!\n");
#else
	fprintf(stderr, "DEBUG: restoring link\n");
	String target;
	char *source;

	DIR *tmp;
	int rel = rf->symval.len;

	/* Craft the target string */
	target.len = rbase.len + rf->name.len + 1;
	if (new_str(target.cstr, target.len) == NULL)
		error(err_msg[ERR_ALLOC]);
	strcat(strcat(strcpy(target.cstr, rbase.cstr), "/"), rf->name.cstr);

	/* Delete the trailer, we need to check if the path exists */
	while (rel-- > rbase.len && target[rel] != '/')
		;
	target[rel] = 0;
	dir_restore(target); /* Try to restore */
	target[rel] = '/';

	/* Craft the source string (the attribute of symval).
	 * We don't care whether the link is broken or not.
	 */
	if (new_str(source, rbase.len + rf->symval.len) == NULL)
		error(err_msg[ERR_ALLOC]);
	strcat(strcpy(source, rbase.cstr), rf->symval);
	/* Symlink */
	symlink(target.cstr, source);
	free(link);
#endif
}

/* It gets called then the file has attribute "c", meaning it's
 * compressed and needs to be extracted.
 * It uses LZW compression IIRC. So, I might need liblzw
 */
static void uncompress() //TODO
{
	return;
}

static void file_restore(Realfile *rf)
{
	fprintf(stderr, "DEBUG: restoring file\n");
	FILE *out;
	FILE *dist;
	DIR *d;
	String path;
	static short lastid;
//	char c;

	/* Craft the path */
	int len = path.len = rbase.len + rf->name.len + 1;

	/* We got the machine field. Add a tag to distinguish... */
	path.len += rf->mach_spec ? 5 : 0;

	if (new_str(path.cstr, path.len) == NULL)
		error(errmsg[ERR_ALLOC]);

	//strcat(strcpy(path.cstr, rbase.cstr), rf->name.cstr);
	strcat(strcat(strcpy(path.cstr, rbase.cstr), "/"), rf->name.cstr);
	if (rf->mach_spec)
		sprintf((path.cstr + path.len - 5), ".%04d", lastid);

	fprintf(stderr, "DEBUG: full path is \"%s\"\n", path.cstr);

	/* Save the mark */
	int mark = path.len;

	/* Check if the directory exist. Otherwise restore it */
	while (mark-- > rbase.len && path.cstr[mark] != '/')
		;
	path.cstr[mark] = '\0';
	path.len = mark;
	if ((d = opendir(path.cstr)) == NULL) {
		dir_restore(path); /* Mhmmm... */
	}
	else
		closedir(d);
	path.cstr[mark] = '/';
	path.len = len;

	/* Load the dist */
	dist = dist_load(rf->dist);
	fprintf(stderr, "DEBUG: dist fd %ud\n", dist);

	/* Seek. In the binary file every name starts with two
	 * escape sequences and the name of the file itself.
	 */
	fseek(dist, 2 + rf->name.len, SEEK_CUR);

	/* Create the output file */
	//if ((out = fopen(rf->path.cstr, "w")) == NULL)
	fprintf(stderr, "DEBUG: file path is %s\n", path.cstr);
	if ((out = fopen(path.cstr, "w")) == NULL) {
		fprintf(stderr, "ERROR: %s\n", strerror(errno));
		error("Can't open out file");
	}
	if (rf->type == 'c') {
		perror("WIP: to implement decompression");
		fseek(dist, rf->size, SEEK_CUR);
		free(path.cstr);
		//uncompress();
		return;
	}

	/* Write the file */
	char *buffer;
	if ((buffer = malloc(rf->size)) == NULL)
		error(errmsg[ERR_ALLOC]);

	fread(buffer, sizeof(char), rf->size, dist);
	fwrite(buffer, sizeof(char), rf->size, out);

	free(buffer);
	fclose(out);

	/* If the file got the machine field, then increase the id for
	 * the next one. We assume that files with the same name are
	 * in the same position within the idb file and the dist.
	 */
	lastid = rf->mach_spec ? lastid + 1 : 0;
	free(path.cstr);
}

/* The main loop */
void undist(FILE* dist)
{
	char *curline = NULL;
	Realfile curfile;

	int entry = 0;
	//int len = 0;
	/* until the end of the file */
	//while (!feof(dist) && dbg < 10) {
	while (!feof(dist)) {
		//fprintf(stderr, "DBG: %d\n", dbg);

		/* Get the line */
		curline = scanline(dist);

		if (curline[0] == '\0') {
			if (curline)
				free(curline);
			return;
		}

		//fprintf(stderr, "DEBUG curline: %s\n", curline);

		/* Create the struct */
		curfile = rf_parse(curline);

		//fprintf(stderr, "DEBUG: restoring \"%s\"\n", curfile.name.cstr);
		fprintf(stdout, "%d. Restoring \"%s\"\n", entry, curfile.name.cstr);

		/* I'll find a way to use function pointers... */
		switch (curfile.type) {
		case 'd':
		{
			String target;
			/* Craft the target string */
			target.len = rbase.len + curfile.name.len + 1;
			if (new_str(target.cstr, target.len) == NULL)
				error(errmsg[ERR_ALLOC]);
			strcat(strcat(strcpy(target.cstr, rbase.cstr), "/"), curfile.name.cstr);
			dir_restore(target);
			free(target.cstr);
		}
			break;
		case 'l':
			//link_restore(&curfile);
			break;
		case 'c':
		case 'f':
			file_restore(&curfile);
			break;
		}

		/* Cleanup */
		rf_clean(&curfile);
		if (curline[0] != '\0')
			free(curline);
		++entry;
	}


}

