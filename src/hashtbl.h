#include "strutils.h"
#include <stdbool.h>

#define BASE 5

struct record {
	/* char *value; */
	uint32_t key;
	FILE *src;	/* The file from which source the dist */
	struct record *next;
};
typedef struct record Record;
typedef struct record Dist;

struct hashtbl {
	Record **bucket;	/* An array of pointers to records */
	uint32_t
		mul_fact,	/* How big is the table, BASE * mul_fact */
		keys,		/* How many records are in the table */
		bucks,		/* The number of buckets */
		split,		/* Current bucket split */
		low_bound,	/* Threshold to shrink */
		up_bound;	/* Threshold to grow */
};
typedef struct hashtbl Hashtbl;

uint32_t hash(Token t);
Hashtbl *tbl_init(void);
void tbl_insert(Hashtbl *tbl, Record *r);
bool tbl_delete(Hashtbl *tbl, uint32_t key);
Record *tbl_lookup(Hashtbl *tbl, uint32_t key);
void tbl_destroy(Hashtbl *tbl);

#define tbl_free(tbl) \
	tbl_destroy((tbl)); \
	free((tbl)); \
	(tbl) = NULL;
