#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "err.h"

const char *errmsg[ERRORS] =
{
	[ERR_IDBSET] = "IDB file already set",
	[ERR_DESTSET] = "Destination already set",
	[ERR_ARG] = "Bogus argument",
	[ERR_ABSPATH] = "Can't resolve absolute path",
	[ERR_IDBOPEN] = "Can't open IDB file",
	[ERR_ALLOC] = "Can't alloc",
	[ERR_REALLOC] = "Can't realloc",
	[ERR_GROW] = "Can't grow array/string",
	[ERR_SHRINK] = "Shrinking of array failed",
	[ERR_PARSE] = "Can't parse. Bad IDB file?",
	[ERR_ATTRIB] = "Can't extract attribute",
	[ERR_FILE] = "Can't open file"
};

void error(const char *msg, ...)
{
	va_list args;

	fprintf(stderr, "Error: ");

	va_start(args, msg);
	vfprintf(stderr, msg, args);
	va_end(args);

	fputc('\n', stderr);
	exit(1);
}
