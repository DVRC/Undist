#include <stdio.h>
#include <stdint.h>

#pragma once

struct string {
	char *cstr;
	uint32_t len;
};

typedef struct string String, Token;

#define BUF_CHUNK 64

char* scanline(FILE *f); /* Ok */
uint32_t tokenize(char *info); /* Ok */
Token nexttok(char *info, int *toks); /* Ok */
String getattr(char *attr); /* Ok */
