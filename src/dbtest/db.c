#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "../macros.h"
#include "../hashtbl.h"
#include "../err.h"

#define DBG

void db_info(Hashtbl *tbl)
{
	printf( "Keys: %d\n"
		"Buckets: %d\n"
		"Current split position: %d\n"
		"Bounds: %d (low) - %d (up)\n"
		"Multiply factor: %d\n",
		tbl->keys,
		tbl->bucks,
		tbl->split,
		tbl->low_bound,
		tbl->up_bound,
		tbl->mul_fact
	);
}

void db_dump(Hashtbl *tbl)
{
	uint32_t i, depth;
	Record *tmp = NULL;

	for (i = 0; i < tbl->bucks; ++i) {
		depth = 0;
		tmp = tbl->bucket[i];

		printf("Bucket %d%s\n", i, tmp ? "" : ": empty\n");
		while (tmp) {
			printf("Item %d\n"
				"Key: %u\n"
				"String: %s\n",
				depth,
				tmp->key,
				tmp->value
			);
			++depth;
			tmp = tmp->next;
		}
	}
}

Token str_acquire(const char *msg)
{
	Token t;
	char c = 0;

	t.len = 0;
	if (new_array(t.str, 1) == NULL)
		error("Couldn't create string");

	printf(msg);
	fflush(stdout);

	/* Now acquire the input */
	do {
		t.str[t.len] = c = getchar();

		++t.len;
		if (resize(t.str, t.len + 1) == NULL)
			error("Couldn't realloc");
	} while (c != '\n' && c != '\0');
	t.str[t.len] = '\0';

	return t;
}

#define str_destroy(tok) \
	free(tok.str); \
	tok.size = 0

void db_insert(Hashtbl *tbl)
{
	Token t = str_acquire("Insert a string: ");


	/* Create the record and insert */
	Record *r = NULL;

	if (new(r) == NULL)
		error("Can't alloc record");

	r->key = pjw_hash(t);
	r->value = t.str;
	r->next = NULL;

	printf("DEBUG KEY: %u\n", r->key);
	printf("DEBUG val: %s\n", r->value);
	tbl_insert(tbl, r);
}

void db_delete(Hashtbl *tbl)
{
	Token t = str_acquire("Insert the string to search: ");

	/* Lookup */
	uint32_t key = pjw_hash(t);

	puts(tbl_delete(tbl, key) ? "Element deleted" : "Element not found");
}

void db_search(Hashtbl *tbl)
{
	Token t = str_acquire("Insert the string to search: ");

	/* Lookup */
	uint32_t key = pjw_hash(t);

	Record *tmp = tbl_lookup(tbl, key);

	if (!tmp)
		puts("Element not found\n");
	else
		printf("Key: %u\n"
			"String: %s\n",
			tmp->key,
			tmp->value
		);
}

int main()
{
	int opt = 0;
	Hashtbl *tbl = NULL;


	void (*db_func[])(Hashtbl *) = {
		db_info,
		db_dump,
		db_insert,
		db_delete,
		db_search
	};

	if ((tbl = tbl_init()) == NULL)
		error("Can't allocate DB");

	do {
		printf( "Menu: \n"
			"1. Print DB info \n"
			"2. Dump the whole DB \n"
			"3. Insert something \n"
			"4. Delete something \n"
			"5. Search something \n"
			"0. Quit\n"
			"> "
		);
		fflush(stdout);
		scanf("%d%*c", &opt);

		if (opt) {
			if (opt < 1 || opt > static_len(db_func))
				printf("Bogus arguments\n");
			else
				db_func[opt-1](tbl);
		}
	} while(opt != 0);

	printf("DEBUG: TBL ADDR: %p\n", tbl);
	fflush(stdout);
	tbl_free(tbl);
	return 0;
}
