#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include "hashtbl.h"
#include "err.h"
#include "macros.h"
#include "strutils.h"

extern Hashtbl *dist_tbl;
extern Token srcpath;
/* If the distribution changed, check if it's already open and loaded.
 * Otherwise, open it and set it to current distribution
 * Update: I'm not dumb, and dynamic hash tables are the solution for
 * this problem.
 */
FILE* dist_load(Token name)
{
	static Dist *d = NULL;

	/* Hash the name */
	uint32_t dist_hash = hash(name);

	/* Didn't change since the last time */
	if (d && d->key == dist_hash)
		return d->src;

	/* Lookup, might be already loaded */
	d = NULL;
	if (d = tbl_lookup(dist_tbl, dist_hash))
		return d->src;

	/* Nevermind, open and insert in the table */
	d = NULL;
	if (new(d) == NULL)
		error(errmsg[ERR_ALLOC]);

	d->next = NULL;
	d->key = dist_hash;

	/* Craft the path */
	{
	char *path = NULL;

	if (new_str(path, name.len + srcpath.len + 1) == NULL)
		error(errmsg[ERR_ALLOC]);

	strcat(strcat(strcpy(path, srcpath.cstr), "/"), name.cstr);

	/* Open the file */
	if ((d->src = fopen(path, "r")) == NULL) {
		free(path);
		error(errmsg[ERR_FILE]);
	}

	/* Destroy the path string */
	free(path);
	}

	/* Insert in the table */
	tbl_insert(dist_tbl, d);

	/* Return the file descriptor */
	return d->src;
}
