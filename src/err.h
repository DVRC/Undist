#pragma once
enum errs {
	ERR_IDBSET,
	ERR_DESTSET,
	ERR_ARG,
	ERR_ABSPATH,
	ERR_IDBOPEN,
	ERR_ALLOC,
	ERR_REALLOC,
	ERR_GROW,
	ERR_SHRINK,
	ERR_PARSE,
	ERR_ATTRIB,
	ERR_FILE,
	ERRORS
};

extern const char *errmsg[ERRORS];

void error(const char *msg, ...);
