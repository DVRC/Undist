/* Implementation of dynamic hash tables, using the "linear hashing" method.
 * More details can be found in the paper from Per-Ake Larson. Other things I
 * know were taken from the "Algorithms and Data Structures" class at UniCa.
 * I had to figure out some things to get a decent implementation.
 *
 *	- DVRC, 2022 Dec 26, 17:02
 *
 * DISCLAIMER: this package is not fault tolerant, since that the control flow
 * is not that optimal. There might be bugs that I missed.
 */

#include <stdlib.h>
#include <stdint.h>

#include "macros.h"
#include "hashtbl.h"
#include "err.h"

#define KNUTH 0.61803
#define PRIME 1048583

/* If needed, I might rewrite the scaffolding to debug the code */

/* PJW hashing method shamelessly taken from Wikipedia and
 * adapted to fix the problem of the poor distribution.
 * It depends on the fact that we're using a 32 bit hash.
 */
uint32_t hash(Token t) /* OK */
{
	uint32_t high;
	union {
		uint32_t i;
		float	 f;
	} h;

	h.i = 0;

	/* We don't need all the string */
	if (t.len > 255)
		t.len = 255;

	while (t.len--) {
		h.i = (h.i << 4) + *(t.cstr)++;
		if (high = h.i & 0xF0000000)
			h.i ^= h.i >> 24;
		h.i &= ~high;
	}

	h.f *= KNUTH;

	return h.i;
}

static
uint32_t getpos(Hashtbl *tbl, uint32_t key)
{
	uint32_t pos = 0;

	pos = key % tbl->up_bound;

	/* The table started growing. If the item cannot be in a certain
	 * bucket, then it must be in a lower position.
	 * Fix: yeah, the arrays are 0 addressed, so sub 1 position...
	 */
	if (tbl->low_bound && pos >= tbl->bucks)
		pos = key % tbl->low_bound;

	return pos;
}

/* Create ex novo the hash table */
Hashtbl *tbl_init(void) /* OK */
{
	Hashtbl *tbl = NULL;

	if (new(tbl) != NULL) {
		/* Alloc the buckets */
		if (new_array(tbl->bucket, BASE) == NULL) {
			free(tbl);
			tbl = NULL;
			return tbl;
		}

		for (int i = 0; i < BASE; ++i)
			tbl->bucket[i] = NULL;

		/* Init the other values */
		tbl->keys = 0;
		tbl->bucks = BASE;
		tbl->mul_fact = 1; /* Mul factor for the high bound */
		tbl->split = 0;
		tbl->low_bound = 0;
		tbl->up_bound = BASE;
	}

	return tbl;
}

/* Grow the table by one cell.
 * If you can't, either a key was eliminated recently or you're out of memory.
 * Good and tested
 */
static void tbl_grow(Hashtbl *tbl)
{

	tbl->bucks++;
	if (resize(tbl->bucket, tbl->bucks) == NULL)
		error("Error growing table");
	tbl->bucket[tbl->bucks - 1] = NULL;

	/* Iterate on every item of the bucket. It stops in two cases:
	 * - there are no more records
	 * - the new head got found (cycle detected!)
	 */
	Record	**cur_item = &(tbl->bucket[tbl->split]),
		**new_buck = NULL,
		*old_list = NULL,
		*tmp = NULL;
	uint32_t pos = 0;

	if (!(*cur_item))
		return;

	while (*cur_item != NULL) {
		/* Extract the current head */
		tmp = *cur_item;
		*cur_item = (*cur_item)->next;
		tmp->next = NULL;

		/* Hash to the new position. If the new position is the
		 * current split, save in a temporary chain */
		new_buck = (pos = getpos(tbl, tmp->key)) == tbl->split
			 ? &old_list : &tbl->bucket[pos];

		/* Descend */
		while (*new_buck)
			new_buck = &(*new_buck)->next;
		*new_buck = tmp;
	}

	/* The saved chain goes where the current split is */
	*cur_item = old_list;
}

/* Insert a new item in the table. */
void tbl_insert(Hashtbl *tbl, Record *r)
{
	/* Grow the table, then increment the split in modulo */
	if (tbl->low_bound && tbl->low_bound + tbl->split == tbl->bucks) {
		tbl_grow(tbl);
		tbl->split = (tbl->split + 1) % tbl->low_bound;
	}

	/* A pointer to the bucket where the item will be inserted*/
	Record **tmp = &tbl->bucket[getpos(tbl, r->key)];

	/* Descend */
	while (*tmp)
		tmp = &(*tmp)->next;
	*tmp = r;

	tbl->keys++;

	/* Double the bounds when the split is again 0 */
	if (tbl->keys > BASE && !tbl->split) {
		tbl->low_bound = tbl->up_bound;
		tbl->mul_fact <<= 1;
		tbl->up_bound = BASE * tbl->mul_fact;
	}
}

/* The upper half is always a location obtained by split + low_bound.
 * So, iterate the upper half, recalculate the position for each key, and
 * insert in the old bucket.
 * Shrink the table to the size expressed by the current lower bound, and
 * half the bounds.
 */
static void tbl_shrink(Hashtbl *tbl)
{
	Record	**cur_buck = NULL,
		**old_buck = NULL,
		*tmp;

	/* Iterate on the upper half. The upper half is not always as big
	 * as the upper bound.
	 */
	for (uint32_t idx = tbl->low_bound;
	     idx < tbl->bucks;
	     ++idx
	) {
		cur_buck = &tbl->bucket[idx];

		/* Until there are records in a bucket, descend */
		while (*cur_buck) {
			/* Extract the head and unlink*/
			tmp = *cur_buck;
			cur_buck = &tmp->next;
			tmp->next = NULL;

			/* Hash to the old position */
			old_buck = &tbl->bucket[tmp->key % tbl->low_bound];

			/* Descend in the old bucket and insert */
			while (*old_buck)
				old_buck = &(*old_buck)->next;
			*old_buck = tmp;
		}
	}

	/* Update the bounds, the number of buckets and shrink the table. */
	if (tbl->low_bound) {
		tbl->bucks = tbl->up_bound = tbl->low_bound;
		tbl->mul_fact >>= 1;
		tbl->low_bound = BASE * (tbl->mul_fact >> 1);

		if (resize(tbl->bucket, tbl->bucks) == NULL)
			error("Error shrinking");
	}
}

/* If the element in the table exist delete it.
 * Then adjust the size of the table if necessary.
 */
bool tbl_delete(Hashtbl *tbl, uint32_t key)
{
	/* No keys, go away! */
	if (!tbl->keys)
		return false;

	/* Descend into the bucket */
	Record	**cur = &tbl->bucket[getpos(tbl, key)];
	Record	*found = NULL;

	while (!found && *cur) {
		if ((*cur)->key == key)
			found = *cur;
		else
			cur = &(*cur)->next;
	}

	if (found) {
		/* Rechain */
		*cur = (*cur)->next;
		free(found);

		/* Adjust accordingly */
		tbl->keys--;
		if (tbl->low_bound) {
			tbl->split = (tbl->split - 1) % tbl->low_bound;

			/* Try to shrink the table */
			if (tbl->keys <= tbl->low_bound)
				tbl_shrink(tbl);
		}
	}

	/* XXX! */
	return (bool)found;
}

/* Lookup and retrieve.
 * If the position is lower than the current split, the item
 * might be in the upper buckets of the table.
 */
Record *tbl_lookup(Hashtbl *tbl, uint32_t key)
{
	/* No keys, go away! */
	if (!tbl->keys)
		return NULL;

	/* Descend into the bucket */
	Record *tmp = tbl->bucket[getpos(tbl,key)];

	while (tmp && tmp->key != key)
		tmp = tmp->next;

	return tmp;
}

static inline
void list_free(Record *tmp)
{
	Record *next = NULL;

	while (tmp) {
		next = tmp->next;
		free(tmp);
		tmp = next;
	}
}

/* OK */
/* It is assumed that the array inside the hash table exists, othewise
 * the table itself would be destroyed by the function tbl_init when it
 * fails, since it returns a NULL pointer
 */
void tbl_destroy(Hashtbl *tbl)
{
	/* No keys, go away! */
	if (!tbl->keys)
		return;

	Record *tmp = NULL;

	/* Dealloc all the records */
	for (int i = 0; i < tbl->bucks; i++) {
		tmp = tbl->bucket[i];
		if (tmp) {
			list_free(tmp);
			tmp = NULL;
		}
	}

	free(tbl->bucket);
	tbl->bucket = NULL;
}
