#pragma once
#include <stdbool.h>
#include <stdint.h>
#include "strutils.h"

/* file */
struct realfile {
	char type; /* d: dir, f: file, c: compr, l: link */
	bool mach_spec;
	//char *perms;
	String perms;
	/* char *owner; */
	/* char *group; */
	String name;
	//String path;
	String dist;	/* the distribution name to open */
	union {
		uint32_t size;	/* file size. If type is 'c', then this
				 * is the size of the compressed file
				 */
		//char *symval;
		String *symval; /* the symlink */
	};
};

typedef struct realfile Realfile;

void undist(FILE* dist);
//Realfile rf_parse(char *line);
//void rf_clean(Realfile *rf);
