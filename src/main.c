#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <sys/stat.h>

#include "macros.h"
#include "restore.h"
#include "hashtbl.h"
#include "err.h"

/* main.c */
enum argstate {
	IDBFILE,
	DEST,
	HELP
};

/* rbase is "root base".
 * Yeah, i am slowly getting the misteries of this file format.
 */
Token rbase = {NULL, 0}, srcpath = {NULL, 0};
FILE *instdb;

Hashtbl *dist_tbl;

void help();

int main(int argc, char *argv[])
{
	char *idbpath = NULL;
	FILE *instdb = NULL;
	enum argstate lastArg;
	if (argc == 1)
		help();

	/* parse all the arguments.
	 * Maybe I need to revamp this part, since it's buggy
	 */
	for (int i = 1; i < argc; i++) {
		/* Argument is a switch for the command */
		if (*argv[i] == '-')
		switch (argv[i][1]) {
		when 'i':
			if (idbpath)
				error(errmsg[ERR_IDBSET]);
			lastArg = IDBFILE;
		when 'o':
			if (rbase.cstr)
				error(errmsg[ERR_DESTSET]);
			lastArg = DEST;
		when 'h':
			lastArg = HELP;
		fallback:
			error(errmsg[ERR_ARG]);
		}
		else switch (lastArg) {
		case IDBFILE:
		{
			char *cut = NULL;

			/* Yeah, I do this because might happen that the shell
			 * doesn't resolve path
			 */
			//printf("DEBUG argv: %d\n", i);
			if ((idbpath = realpath(argv[i], NULL)) == NULL)
				error(errmsg[ERR_ABSPATH]);
			//fprintf(stderr, "DEBUG input path: %s\n", idbpath);

			if (strstr(idbpath, ".idb") == NULL)
				error("Not an IDB file");

			/* Get also the path, we will need it later... */
			srcpath.len = strlen(idbpath);
			while (srcpath.len-- && idbpath[srcpath.len] != '/')
				;
			if (new_str(srcpath.cstr, srcpath.len) == NULL)
				error(errmsg[ERR_ALLOC]);

			strncpy(srcpath.cstr, idbpath, srcpath.len);
		}
			break;
		case DEST: /* Output directory */
		{
			/* Try resolve the path */


			/* The path doesn't exist yet, let's create it */
			int suffix_len = strlen(argv[i]);
			int rel = suffix_len;

			while (rel-- && argv[i][rel] != '/')
				;
			argv[i][rel] = '\0';	/* Truncate the slash */

			char *tmp = realpath(argv[i], NULL);
			//fprintf(stderr, "DEBUG resolved path: %s\n", tmp);
			int prefix_len = strlen(tmp);

			if (new_str(rbase.cstr, (prefix_len + suffix_len)) == NULL)
				error(errmsg[ERR_ALLOC]);

			argv[i][rel] = '/';	/* Restore the slash */
			strcpy(rbase.cstr, tmp);

			while (rel < suffix_len)
				rbase.cstr[prefix_len++] = argv[i][rel++];
			//fprintf(stderr, "DEBUG constructed path: %s\n", rbase.cstr);
			rbase.len = strlen(rbase.cstr);
			free(tmp);

			/* Check if the directory exist, otherwise create it */
			DIR *d;
			if ((d = opendir(rbase.cstr)) == NULL)
				mkdir(rbase.cstr, 0755);
			else
				closedir(d);

		}
			break;
		case HELP:
			help();
			return 0;
		}
	}

	/* Check that we have all we need */
	if (!idbpath || !rbase.cstr) {
		help();
		error("wrong args");
	}

	/* Try open the file */
	if ((instdb = fopen(idbpath, "r")) == NULL) {
		fprintf(stderr, errmsg[ERR_IDBOPEN]);
		goto cleanup;
	}

	/* Initialize the hashtbl */
	if ((dist_tbl = tbl_init()) == NULL) {
		fprintf(stderr, "Error initializing table. %s\n",
			errmsg[ERR_ALLOC]);
		goto cleanup;
	}

	/* Check whether the output directory exists, otherwise create it */

	/* parse_and_extract */
	printf("IDB path: %s\n", idbpath);
	printf("Source path: %s\n", srcpath.cstr);
	printf("Destination path: %s\n", rbase.cstr);
	printf("Instdb fd: %d\n", instdb);
	printf("dist_tbl installed at %p\n", dist_tbl);
	undist(instdb);
	/* Cleanup */
cleanup:
	free(idbpath);
	idbpath = NULL;
	free(srcpath.cstr);
	srcpath.cstr = NULL;

	return 0;
}

void help()
{
	puts("Usage: undist <arg> <param> ...");
	puts("-i: idb database. You must have both the .idb and the archive");
	puts("-o: output directory");
	puts("-h: display this help message");
	exit(0);
}
