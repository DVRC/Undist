/* Resolve a path of this form
 * /a/b/c/d/...
 *
 */
static char* parse_absolute(char *path)
{
	char *resolved;
	short level = 0; /* Cannot go under 0 */
}

/* Resolve a path of this form:
 * (.|..)a/b/c/d
 */
static char* parse_relative(char *path)
{
	char *resolved;
	short level = 0;
}

char* resolvepath(char *path)
{
	char *resolved;
	/* Try resolve the path naturally */
	if ((resolved = realpath(path, NULL)) != NULL)
		return resolved;

	/* The path doesn't exist yet, let's analyze it */
	return *path == '/' ? parse_fromroot(path) : parse_relative(path);
}
