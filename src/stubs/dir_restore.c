#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <errno.h>

struct token {
	char *str;
	unsigned len;
};
typedef struct token Token;

Token rbase;

void dir_restore(char *path);

int main()
{
	char cbase[] = "/home/dvrc/src/Undist/src/stubs";
	char cpath[] = "/home/dvrc/src/Undist/src/stubs/iris/test";
	Token path;

	rbase.str = malloc(sizeof(cbase));
	strcpy(rbase.str, cbase);
	rbase.len = sizeof(cbase);

	path.str = malloc(sizeof(cpath));
	strcpy(path.str, cpath);
	path.len = sizeof(cpath);

	dir_restore(path.str);
	free(rbase.str);
	free(path.str);
	return 0;
}

void dir_restore(char *path)
{
	DIR *d;
	int len = strlen(path);
	int rel = len;
	bool exists = false;

	/* This is pretty clever: we know which is the rbase and where
	 * we are going to extract the whole dist. So, there is for sure
	 * a common prefix.
	 */
	printf("len of path: %d\n"
		"len of rbase: %d\n", len, rbase.len);
	while (((d = opendir(path)) == NULL) && rel > rbase.len) {
		printf("%s doesn't exist!\n", path);

		while (rel-- > rbase.len && path[rel] != '/')
			;
		path[rel] = '\0';
	}

	printf("%s exists!\n", path);

	while (rel < len) {
		if (path[rel] == '\0' && rel < len) {
			path[rel] = '/';
			mkdir(path, 0700); /* This can fail ! */
			printf("New path: %s\n", path);
		}
		printf("Current char: %c\n", path[rel]);
		rel++;
	}


/*	chmod(rf->path, atoi(rf->perms));*/

}
