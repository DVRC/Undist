#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

struct token {
	char *str;
	int len;
};
typedef struct token Tok;

void dir_restore(Tok path)
{
	/* Open the dir.
	 * If it can open, return.
	 * Otherwise, modify the path to check the parent.
	 * Then, restore
	 */
	DIR *d;
/*	if (d = opendir(path)) {
		closedir(d);
		return;
	}
*/

	/* Search for the previous slash, replace it with a terminator
	 * and try open that dir.
	 * The root dir is guaranteed to exist
	 */
	int rel = path.len;
/*	while ((d = opendir(path)) == NULL) {
		while (rel-- > 1 && path.str[rel] != '/');
			;
		path.str[rel] = '\0';
		printf("Cur path: %s\n", path.str);
	} */
	while (rel-- > 1) {
		printf("Index: %d\n", rel);
		if (path.str[rel] == '/')
			path.str[rel] = '\0';
		printf("Cur path: %s\n", path.str);
	}

	rel++;
	while (rel < path.len) {
		printf("Index: %d\n", rel);
		if (path.str[rel] == '\0') {
			path.str[rel] = '/';
			/* Should inherit the parent permissions */
			printf("Restored path: %s\n", path.str);
		}
		rel++;
	}
}

int main()
{
	char *str = "/home/dvrc/src/andrew/src";
	Tok path;
	path.len = strlen(str);
	path.str = malloc(path.len + 1);
	strcpy(path.str, str);

	dir_restore(path);
	return 0;
}
