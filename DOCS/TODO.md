# Done
- Implemented the dist DB
- Implemented the functions to read file attributes
- Implemented the functions to restore the files
- Implemented the functions to restore folders

# TODO
- Test and finalize the function to restore the symlinks (WIP)
- Find a way to handle more efficiently the mach() field.  
  For this I will have to recover the trailer from the package string.  
  (Ex: news\_server.CLOVER1)
- Find a way to handle dists more efficiently (maybe linear hashing is overkill...)

# TOFIX:
- argument parsing causes segfault if there are too many arguments

# To check
- check if the path gets resolved correctly in every case, on any shell
- check it doesn't crash
- check it extracts IRIX 3 dists
- check it extracts IRIX 4 dists
- check it extracts IRIX 5 dists

# Bonus
- Implement features to restore a complete dist with all the parameters (a sort of
  installer/packet manager?)

# Internals of the utility I'm writing and caveats (not part of the specification)
- c: compressed file. It marks the file for decompression when extracted 
- m: manpage
- if the dist name is "\*.sw.\*", then the distribution is just \*.sw; Otherwise it's the whole string
- entries with the mach() attribute get extracted with a number appended, to differentiate.  
