# Anatomy of an IDB file (from the IRIX 3 distfiles)
I suppose IDB stands for "Install DataBase".
Here I describe all the attributes I found inside the IRIX 3.3 .idb files and their meaning.

## Main IDB file entries
- <type> :  d -> directory | f -> file | l -> link
- <4 digits> : octal string, UNIX permissons
- <owner> : string
- <group> : string
- <name> : string, name of the file/directory, including the directory where resides  
- <package> : string, can be ignored
- <dist> : string, source distribution

## Entries with a variable field (attributes)
### System
- config() : suggested | update | noupdate
- mach() : machine, multiple key=value (CPUBOARD, GFXBOARD, SUBGR)
- f() : int, likely a tag for the file. Check this: [tag](https://nixdoc.net/man-pages/IRIX/man1/tag.1.html)

### Shell scripts
Shell scripts are enclosed by double quotes
- preop() : shell script, execute before installing
- postop() : shell script, execute after install
- exitop() : shell script, execute after the whole install process

### Size or link
- size() : int, the size of the file
- cmpsize() : compressed file size. If present, then the archive should be uncompressed
- sum() : int, just a checksum
- symval() : string, the file from which the link (specified by `<dest>`)should be created.  
  If it's just a filename, then it means is in the same directory as in destination

### Other attributes
- noshare : duplicate file in case you have an application that runs from a server for each workstation

### Variables found inside postop/exitop scripts
- $rbase: likely stands for "root base", where the whole distribution will be installed
- @$instmode and X$instmode : mistery
- $vhdev : mistery

## Attributes found in later specifications (IRIX 5, IRIX 6)
- prereq() : dependency
- shadow : installs on reboot
- nostrip : don't strip the installed application

## How I found the tokens
`strings *.idb | cut -d ' ' -f 9- | tr ' ' '\n' | grep \( | cut -d '(' -f 1 | sort | uniq`

### Result on the IRIX 3.3 set:
- cmpsize
- config
- exitop
- f
- mach
- postop
- preop
- size
- sum
- symval
- noshare

## Structure of the binary distribution (usually .sw):
Every file is separated in this way:
- An header composed of a NULL character, a random character (^F, ^G, ^L, ^H, who knows  
  how many there are), and the <name> field.  
  Example:
  ^@^Hbin/news\_server
- the file itself, of length <size> or <cmpsize>

## Oddity
For an unknown reason, at the end of both the .idb and .sw files there are many NULL characters.
