# Undist
Tool to extract IRIX distfiles.
Currently my main focus is to extract IRIX 3.3 distfiles. I might add support
in the future to extract distfiles of later IRIX versions.

## Dependencies:
- Not yet

## Similar tools I found that helped me write my own version:
- [camthesaxman and shawlucas](https://github.com/shawlucas/irixswextract).
- [swextract](https://github.com/misuchiru03/swextract)
- [Mydb](http://persephone.cps.unizar.es/~spd/src/other/mydb.c)
- [ipack](http://web.mit.edu/install/irix/lib/ipack)
- [sgix](https://github.com/depp/sgix)
